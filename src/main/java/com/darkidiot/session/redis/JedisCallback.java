package com.darkidiot.session.redis;

import redis.clients.jedis.Jedis;

/**
 * 回调接口
 * session attribute持久化到redis
 * Copyright (c) for darkidiot
 * Date:2017/4/14
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
interface JedisCallback<V> {

    V execute(Jedis jedis);
}
