package com.darkidiot.session.conf;

/**
 * Copyright (c) for darkidiot
 * Date:2017/4/16
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
public enum SerializeType {
    /** json方式序列化 */
    json,
    /** 对象流方式序列化 */
    binary;

    public static SerializeType value(String type) {
        SerializeType[] values = SerializeType.values();
        for (SerializeType value : values) {
            if (value.toString().equals(type)) {
                return value;
            }
        }
        return null;
    }
}
