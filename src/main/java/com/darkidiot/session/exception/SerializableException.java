package com.darkidiot.session.exception;

/**
 * 序列化异常类
 * Copyright (c) for darkidiot
 * Date:2017/4/14
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
public class SerializableException extends RuntimeException {

    private Code code;

    private SerializableException(String msg) {
        super(msg);
    }

    public SerializableException(Code code, String msg) {
        super(msg);
        this.code = code;
    }

    public SerializableException(Code code, String msg, Throwable throwable) {
        super(msg, throwable);
        this.code = code;
    }

    private static class SerializeException extends SerializableException {
        SerializeException(String msg) {
            super(Code.SERIALIZE_ERROR, msg);
        }
    }

    private static class DeserializeException extends SerializableException {
        DeserializeException(String msg) {
            super(Code.DESERIALIZE_ERROR, msg);
        }
    }

    public enum Code {
        /** 序列化异常 */
        DESERIALIZE_ERROR,
        /** 反序列化异常 */
        SERIALIZE_ERROR
    }

    public static SerializableException create(Code code, String msg) {
        switch (code) {
            case DESERIALIZE_ERROR:
                throw new DeserializeException(msg);
            case SERIALIZE_ERROR:
                throw new SerializeException(msg);
            default:
                throw new SerializableException(msg);
        }
    }
}
